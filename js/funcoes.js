var mapa;

function initMap() {

  var configuracoes = {
    center: {lat: -25.45212754, lng: -49.25276682},
    zoom: 15
  }
      
  mapa = new google.maps.Map(document.getElementById('map'), configuracoes);

  var marcador = new google.maps.Marker({
    position: {lat: -25.45212754, lng: -49.25276682},
    title: "PUCPR",
    map: mapa
  });

}

function alerta(){
  alert("mensagem enviada com sucesso");
}

function registro(){
  alert("Conta criada com sucesso, seja muito bem-vindo ao enquadrados!");
  window.location.href = 'index.html';
}

function login(){
  alert("Logado com sucesso!");
  window.location.href = 'index.html';
}

function AddCarrinho(produto, qtd, valor, posicao)
	{
		localStorage.setItem("produto" + posicao, produto);
		localStorage.setItem("qtd" + posicao, qtd);
		valor = valor * qtd;
		localStorage.setItem("valor" + posicao, valor);
		alert("Produto adicionado ao carrinho!");
  }
  
  var total = 0; // variável que retorna o total dos produtos que estão na LocalStorage.
  var i = 0;     // variável que irá percorrer as posições
  var valor = 0; // variável que irá receber o preço do produto convertido em Float.
  
  for(i=1; i<=99; i++) // verifica até 99 produtos registrados na localStorage
  {
    var prod = localStorage.getItem("produto" + i + ""); // verifica se há recheio nesta posição. 
    if(prod != null) 
    {	
      // exibe os dados da lista dentro da div itens
      document.getElementById("itens").innerHTML += localStorage.getItem("qtd" + i) + " x ";
      document.getElementById("itens").innerHTML += localStorage.getItem("produto" + i);
      document.getElementById("itens").innerHTML += " ";
      document.getElementById("itens").innerHTML += "R$: " + localStorage.getItem("valor" + i) + "<hr>";
      
      // calcula o total dos recheios
      valor = parseFloat(localStorage.getItem("valor" + i)); // valor convertido com o parseFloat()
      total = (total + valor); // arredonda para 2 casas decimais com o .toFixed(2)
    }
  } 
  // exibe o total dos recheios
  document.getElementById("total").innerHTML = total.toFixed(2); 